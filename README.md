# Hello-express

This project contains REST API with following features:

- Welcome endpoint
- Multiply endpoint

## Getting started

First install the depedencies

```sh 
npm i
```

Start the REST API service:

`node app.js`

Or in the latest Node25 version (>=20):

`node --watch app.js`

